// Fill out your copyright notice in the Description page of Project Settings.


#include "ArenaProjectile.h"
#include <Components/SphereComponent.h>
#include <GameFramework/ProjectileMovementComponent.h>
#include "ArenaCharacter.h"
#include <Kismet/GameplayStatics.h>

// Sets default values
AArenaProjectile::AArenaProjectile()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	sphereComponent = CreateDefaultSubobject<USphereComponent>(FName(TEXT("Sphere")));
	RootComponent = sphereComponent;
	sphereComponent->Mobility = EComponentMobility::Movable;
	sphereComponent->SetSphereRadius(10.0f);
	//sphereComponent->SetCollisionProfileName(TEXT("OverlapAllDynamic"));
	//sphereComponent->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel1);
	sphereComponent->SetCollisionProfileName(TEXT("BlockAll"));
	//sphereComponent->AreaClass = FNavigationSystem::GetDefaultObstacleArea();

	meshComponent = CreateDefaultSubobject<UStaticMeshComponent>(FName(TEXT("Mesh")));
	meshComponent->AttachTo(sphereComponent);
	meshComponent->SetCollisionProfileName(TEXT("NoCollision"));

	ConstructorHelpers::FObjectFinder<UStaticMesh> meshAsset(TEXT("/Engine/BasicShapes/Sphere"));
	if (meshAsset.Succeeded()) {
		mesh = meshAsset.Object;
		meshComponent->SetStaticMesh(mesh);
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("Mesh not found!"))
	}
	ConstructorHelpers::FObjectFinder<UMaterial> materialAsset(TEXT("/Game/Materials/Mat_Projectile"));
	if (materialAsset.Succeeded()) {
		material = materialAsset.Object;
		materialInstance = UMaterialInstanceDynamic::Create(material, meshComponent);
		meshComponent->SetMaterial(0, materialInstance);
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("Material not found!"))
	}
	meshComponent->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
	meshComponent->SetRelativeScale3D(FVector(0.5f, 0.05f, 0.05f));

	movementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(FName(TEXT("ProjectileMovement")));
	movementComponent->SetUpdatedComponent(sphereComponent);
	movementComponent->InitialSpeed = 1000.0f;
	movementComponent->ProjectileGravityScale = 0.0f;

}

// Called when the game starts or when spawned
void AArenaProjectile::BeginPlay()
{
	Super::BeginPlay();

	sphereComponent->OnComponentHit.AddDynamic(this, &AArenaProjectile::OnHit);

}

void AArenaProjectile::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	AActor* MyOwner = GetOwner();
	if (!MyOwner) {
		return;
	}

	if (OtherActor && OtherActor != this && OtherActor != MyOwner) {
		UGameplayStatics::ApplyDamage(OtherActor, Damage, MyOwner->GetInstigatorController(), this, UDamageType::StaticClass());
	}

	Destroy();
}

// Called every frame
void AArenaProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

