// Fill out your copyright notice in the Description page of Project Settings.

#include "ArenaHero.h"
#include "ArenaWeapon.h"
#include <GameFramework/SpringArmComponent.h>
#include <Camera/CameraComponent.h>
#include "Kismet/KismetMathLibrary.h"
#include <Kismet/GameplayStatics.h>

AArenaHero::AArenaHero() {

	springArm = CreateDefaultSubobject<USpringArmComponent>(FName(TEXT("SpringArm")));
	springArm->AttachTo(GetRootComponent());
	springArm->SetRelativeRotation(FRotator(-90.0f, 0.0f, 0.0f));
	springArm->TargetArmLength = 1000.0f;
	springArm->bDoCollisionTest = false;
	springArm->bInheritPitch = false;
	springArm->bInheritYaw = false;
	springArm->bInheritRoll = false;

	camera = CreateDefaultSubobject<UCameraComponent>(FName(TEXT("Camera")));
	camera->AttachTo(springArm);

}

// Called when the game starts or when spawned
void AArenaHero::BeginPlay()
{
	Super::BeginPlay();
}

void AArenaHero::StartFire() {
	weapon->StartFire();
}

void AArenaHero::StopFire() {
	weapon->StopFire();
}