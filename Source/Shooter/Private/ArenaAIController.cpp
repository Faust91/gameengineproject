// Fill out your copyright notice in the Description page of Project Settings.


#include "ArenaAIController.h"
#include <Kismet/GameplayStatics.h>
#include <GameFramework/Controller.h>
#include "ArenaCharacter.h"

void AArenaAIController::ChasePlayer_Implementation()
{
	APawn* goal = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
	MoveToActor(goal, 5.0f, false);
}

bool AArenaAIController::IsDead() const
{
	AArenaCharacter* ControlledCharacter = Cast<AArenaCharacter>(GetPawn());
	if (ControlledCharacter) {
		return ControlledCharacter->IsDead();
	}
	return true;
}

void AArenaAIController::BeginPlay()
{
	Super::BeginPlay();

	UKismetSystemLibrary::K2_SetTimer(this, TEXT("ChasePlayer"), 1.0f, true);
}
