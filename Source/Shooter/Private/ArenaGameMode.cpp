// Fill out your copyright notice in the Description page of Project Settings.


#include "ArenaGameMode.h"
#include "ArenaHero.h"
#include <Kismet/GameplayStatics.h>
#include <Engine/TargetPoint.h>
#include <Kismet/KismetArrayLibrary.h>
#include "ArenaEnemy.h"
#include <EngineUtils.h>
#include "ArenaAIController.h"
#include "ArenaPlayerController.h"

AArenaGameMode::AArenaGameMode() {
	DefaultPawnClass = AArenaHero::StaticClass();
	PlayerControllerClass = AArenaPlayerController::StaticClass();
}

void AArenaGameMode::PawnKilled(APawn* PawnKilled)
{
	APlayerController* PlayerController = Cast<APlayerController>(PawnKilled->GetController());
	if (PlayerController) {
		EndGame(false);
	}
	else {
		numEnemies--;
	}
	for (AArenaAIController* Controller : TActorRange<AArenaAIController>(GetWorld())) {
		if (!Controller->IsDead()) {
			return;
		}
	}
	EndGame(true);
}

void AArenaGameMode::BeginPlay() {
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATargetPoint::StaticClass(), spawnPoints);
	FTimerHandle UnusedHandle;
	GetWorldTimerManager().SetTimer(UnusedHandle, this, &AArenaGameMode::SpawnWave, 2.0f, true);
}

void AArenaGameMode::SpawnWave() {
	if (numEnemies < maxEnemies) {
		numEnemies++;
		int32 randomIndex = FMath::RandRange(0, spawnPoints.Num() - 1);
		ATargetPoint* spawnPoint = Cast<ATargetPoint>(spawnPoints[randomIndex]);
		if (UKismetSystemLibrary::IsValid(spawnPoint)) {
			GetWorld()->SpawnActor<AArenaEnemy>(AArenaEnemy::StaticClass(), spawnPoint->GetActorTransform());
		}
	}
}

void AArenaGameMode::EndGame(bool bIsPlayerWinner)
{
	for (AController* Controller : TActorRange<AController>(GetWorld())) {
		bool bIsWinner = Controller->IsPlayerController() == bIsPlayerWinner;
		Controller->GameHasEnded(Controller->GetPawn(), bIsWinner);
	}
}