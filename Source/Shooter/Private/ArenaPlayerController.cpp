// Fill out your copyright notice in the Description page of Project Settings.


#include "ArenaPlayerController.h"
#include <Kismet/GameplayStatics.h>
#include <Kismet/KismetMathLibrary.h>
#include "ArenaHero.h"

void AArenaPlayerController::BeginPlay() {
	Super::BeginPlay();
}

// Called to bind functionality to input
void AArenaPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAxis("MoveUp", this, &AArenaPlayerController::MoveAlongYAxis);
	InputComponent->BindAxis("MoveRight", this, &AArenaPlayerController::MoveAlongXAxis);
	InputComponent->BindAxis("LookUp", this, &AArenaPlayerController::LookAlongYAxis);
	InputComponent->BindAxis("LookRight", this, &AArenaPlayerController::LookAlongXAxis);
}

void AArenaPlayerController::MoveAlongYAxis(float value) {
	GetCharacter()->AddMovementInput(FVector::ForwardVector, value, false);
}

void AArenaPlayerController::MoveAlongXAxis(float value) {
	GetCharacter()->AddMovementInput(FVector::RightVector, value, false);
}

void AArenaPlayerController::LookAlongYAxis(float value) {
	lookY = value;
}

void AArenaPlayerController::LookAlongXAxis(float value) {

	lookX = value;
}

void AArenaPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector vect = FVector(lookY, lookX, 0.0f);

	if (vect.Size() > 0.5f) {
		UGameplayStatics::GetPlayerController(GetWorld(), 0)->SetControlRotation(UKismetMathLibrary::MakeRotFromX(vect));
		Cast<AArenaHero>(GetCharacter())->StartFire();
	}
	else {
		Cast<AArenaHero>(GetCharacter())->StopFire();
	}

}