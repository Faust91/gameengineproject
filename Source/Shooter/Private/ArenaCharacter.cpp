// Fill out your copyright notice in the Description page of Project Settings.


#include "ArenaCharacter.h"
#include "ArenaWeapon.h"
#include "ArenaGameMode.h"
#include "HealthComponent.h"

// Sets default values
AArenaCharacter::AArenaCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	GetMesh()->SetRelativeLocation(FVector(0.0f, 0.0f, -90.0f));
	GetMesh()->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));

	ConstructorHelpers::FObjectFinder<USkeletalMesh> meshAsset(TEXT("/Game/AnimationAssets/UE4_Mannequin/Mesh/SK_Mannequin"));
	if (meshAsset.Succeeded()) {
		GetMesh()->SetSkeletalMesh(meshAsset.Object);
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("Mesh not found!"))
	}

	ConstructorHelpers::FClassFinder<UAnimInstance> animationAssetClass(TEXT("/Game/AnimationAssets/GameBaseCharacterAnimBP"));
	if (animationAssetClass.Succeeded()) {
		GetMesh()->SetAnimClass(animationAssetClass.Class);
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("Animation class not found!"))
	}

	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("Health Component"));

}

bool AArenaCharacter::IsDead() const
{
	return HealthComponent->IsDead();
}

// Called when the game starts or when spawned
void AArenaCharacter::BeginPlay()
{
	Super::BeginPlay();

	weapon = GetWorld()->SpawnActor<AArenaWeapon>(GetMesh()->GetComponentTransform().GetLocation(), GetMesh()->GetComponentTransform().GetRotation().Rotator());
	FAttachmentTransformRules weaponRules = FAttachmentTransformRules(EAttachmentRule::SnapToTarget, false);
	weapon->AttachToComponent(GetMesh(), weaponRules, FName(TEXT("GunSocket")));
	weapon->SetOwner(this);

}

// Called every frame
void AArenaCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AArenaCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}


void AArenaCharacter::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

}
