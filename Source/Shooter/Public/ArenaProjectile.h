// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ArenaProjectile.generated.h"

class USphereComponent;
class UStaticMeshComponent;
class UProjectileMovementComponent;

UCLASS()
class SHOOTER_API AArenaProjectile : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AArenaProjectile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
		void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	UPROPERTY(EditAnywhere)
		USphereComponent* sphereComponent;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* meshComponent;
	UPROPERTY(EditAnywhere)
		UProjectileMovementComponent* movementComponent;
	UPROPERTY()
		UStaticMesh* mesh;
	UPROPERTY()
		UMaterial* material;
	UPROPERTY()
		UMaterialInstanceDynamic* materialInstance;

	UPROPERTY(EditAnywhere)
		float Damage = 25.0f;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
