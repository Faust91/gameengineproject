// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include <Engine/EngineTypes.h>
#include "ArenaWeapon.generated.h"

class USkeletalMeshComponent;
class UArrowComponent;

UCLASS()
class SHOOTER_API AArenaWeapon : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AArenaWeapon();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
		USkeletalMeshComponent* skeletalMesh;
	UPROPERTY(EditAnywhere)
		UArrowComponent* arrow;
	UPROPERTY()
		USkeletalMesh* mesh;
	UPROPERTY()
		UMaterialInstance* materialInstance;
	UPROPERTY(EditAnywhere, Category = "Weapon")
		float fireRate = 0.25f;
	UPROPERTY(EditAnywhere, Category = "Weapon")
		float fireDelay = 0.25f;
	UPROPERTY(EditAnywhere, Category = "Weapon")
		FTimerHandle fireTimer;

	bool bDoOnce = true;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintNativeEvent)
		void StartFire();
	UFUNCTION(BlueprintNativeEvent)
		void StopFire();
	UFUNCTION(BlueprintNativeEvent)
		void Fire();

};
