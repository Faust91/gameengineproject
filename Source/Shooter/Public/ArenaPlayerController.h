// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "ArenaPlayerController.generated.h"

/**
 *
 */
UCLASS()
class SHOOTER_API AArenaPlayerController : public APlayerController
{
	GENERATED_BODY()

public:

	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:

	virtual void BeginPlay() override;

	virtual void SetupInputComponent() override;

	void MoveAlongYAxis(float value);
	void MoveAlongXAxis(float value);
	void LookAlongYAxis(float value);
	void LookAlongXAxis(float value);

	float lookY = 0.0f;
	float lookX = 0.0f;
};
