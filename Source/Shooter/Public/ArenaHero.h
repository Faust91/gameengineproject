// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ArenaCharacter.h"
#include "ArenaHero.generated.h"

class AArenaWeapon;
class USpringArmComponent;
class UCameraComponent;

/**
 *
 */
UCLASS()
class SHOOTER_API AArenaHero : public AArenaCharacter
{
	GENERATED_BODY()

public:
	AArenaHero();

	void StartFire();

	void StopFire();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
		USpringArmComponent* springArm;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
		UCameraComponent* camera;

};
