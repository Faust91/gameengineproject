// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ArenaCharacter.h"
#include "ArenaEnemy.generated.h"

class AArenaWeapon;

/**
 *
 */
UCLASS()
class SHOOTER_API AArenaEnemy : public AArenaCharacter
{
	GENERATED_BODY()

public:
	AArenaEnemy();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void OnConstruction(const FTransform& Transform) override;

};
